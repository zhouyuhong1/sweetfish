<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String sort_id=request.getParameter("sort_id");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

	<head>
		<title>分类列表-甜鱼二手网</title>
		<link rel="stylesheet" href="css/public.css" />
		<link rel="stylesheet" href="css/category.css" />
		<link rel="stylesheet" href="css/layui.css" />
		<link rel="stylesheet" href="css/sortgood.css" />
		<script type="text/javascript" src="js/jquery.min.js" ></script>
	</head>
		<body>
			<%@ include file="header.jsp" %>
			<jsp:include page="headerSearh.jsp"></jsp:include>
			
			<div id="main" class="clearfix">
				<div class="cate-name-list" id="cate-name-list"></div>
				<div style="margin-top: 20px;"></div>
				<div class="list-body">
					<ul class="clearfix hei" id="seachBySort">
						
					</ul>
				</div>
			</div>
	<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript">
					$(function(){
						/*导入分类*/
						$.getJSON("publish/getSorts",function(data){
						
								var str="<a class='sortTop' value='-1'>全部分类</a>";
								var sortid= <%= sort_id==null?1:sort_id%>;
							$(data).each(function(){
								if(this.sort_id==sortid){
									str+="<a class='sortTop active' value='"+this.sort_id+"'>"+this.sort_name+"</a>";
								}
								else{str+="<a class='sortTop' value='"+this.sort_id+"'>"+this.sort_name+"</a>";}
								/* str存在空值的可能！*/
								$("#cate-name-list").html(str);
								$.getJSON("seach/queryBySort",{"sort_id":sortid},function(data){
											var str = "";
											$(data).each(function(){
												str+="<li><a class='good-image goods' target='_blank' value='"+this.g_id+"'>"+
												"<img src='"+this.g_img+"' alt='"+this.g_name+"'></a>"+
											"<a class='good-title goods' target='_blank'>"+this.g_name+"</a><span class='good-price'>" + this.g_price + "</span><span class='pub-time fr'>发布于 " + this.create_time +"</li>";
											});
											$("#seachBySort").html(str);
										});
							});
							$("#goSort").html(str);
						});
						
						$("#cate-name-list").on("click",".sortTop",function(){
							var sort_id = $(this).attr("value");
							if(sort_id!=-1){
							$(".sortTop").each(function(){
								if($(this).attr("value")!=sort_id){
								if($(this).hasClass("active")){
									$(this).removeClass("active");
									}
								}else{
									if(!$(this).hasClass("active")){
										$(this).addClass("active");
										$.getJSON("seach/queryBySort",{"sort_id":sort_id},function(data){
											var str = "";
											$(data).each(function(){
												str+="<li><a class='good-image goods' target='_blank' value='"+this.g_id+"'>"+
												"<img src='"+this.g_img+"' alt='"+this.g_name+"'></a>"+
											"<a class='good-title goods' target='_blank'>"+this.g_name+
										"</a><span class='good-price'>" + this.g_price + "</span><span class='pub-time fr'>发布于 " + this.create_time +"</li>";
											});
											$("#seachBySort").html(str);
										});
									}
								}
							});
							}
						});
						
							$("#seachBySort").on("click",".goods",function(){
							var g_id = $(this).attr("value");
							window.location.href="shopshow.jsp?g_id="+g_id;
							window.event.returnValue=false; 
			});
	});
				</script>
		</body>

</html>