<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
<title>甜鱼二手网-发布二货</title>
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/layui.css">
<link rel="stylesheet" href="css/public.css?v=0.1">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="main" class="clearfix">
		<h2 class="btya">发布 二货</h2>
		<!-- 发布表单 -->
		<form action="publish/create" method="post"
			enctype="multipart/form-data">
			<div class="publish-main layui-form layui-form-pane">
				<div class="imgload">
					<span>上传图片</span>
					<div id="imgPreview" style="margin-left:130px;">
						<div id="prompt3">
							<span id="imgSpan">点击上传 <i
								class="aui-iconfont aui-icon-plus"></i>
							</span>
							<!--AUI框架中的图标-->
							<input type="file" id="file" name="file" class="filepath"
								onchange="changepic(this)"
								accept="image/jpg,image/jpeg,image/png,image/PNG">
							<!--当vaule值改变时执行changepic函数，规定上传的文件只能是图片-->
						</div>
						<img src="#" id="img3" />
					</div>
				</div>
			</div>
			<div class="layui-form-item input-line">
				<label class="layui-form-label">商品名称</label>
				<div class="layui-input-inline" style="width: 390px;">
					<input type="text" name="name" placeholder="请输入商品名称"
						class="layui-input goods-title" id="spmc">
				</div>
				<div class="layui-form-mid layui-word-aux">14个字以内</div>
			</div>
			<div class="layui-form-item layui-form-text input-line">
				<label class="layui-form-label good-description">商品详情</label>
				<div class="layui-input-block">
					<textarea placeholder="请输入商品详情" style="width:50%;resize: none;"
						name="text" class="layui-textarea goods-des" id="spxq"></textarea>
				</div>
			</div>
			<div class="layui-form-item input-line" id="price">
				<label class="layui-form-label" style="margin-left:-28px">价格</label>
				<div class="layui-input-inline">
					<input type="text" name="price" placeholder="请输入价格"
						class="layui-input price" style="margin-left:28px" id="spprice">
				</div>
				<div class="layui-form-mid layui-word-aux">元</div>
			</div>
			<div>
				<br> <span style="padding-left:38px;">分类</span> <br> <br>
				<div>
					<select style="margin-left:112px;margin-top:-40px;" id="select"
						name="sort"></select>
				</div>
			</div>

			<div class="layui-form-item input-line" id="address">
				<label class="layui-form-label">交易地址</label>
				<div class="layui-input-block">
					<input type="text" name="address" placeholder="请输入交易地址"
						style="width:80%" class="layui-input goods-address" id="jyaddress">
				</div>
			</div>
			<div class="layui-form-item input-line" id="phoneNumber">
				<label class="layui-form-label" style="margin-left:-15px">手机号</label>
				<div class="layui-input-inline">
					<input type="text" name="phone" placeholder="请输入手机号"
						class="layui-input phone-number" value="" style="margin-left:15px"
						id="phonenum">
				</div>
				<br>
				<div class="publish-submit">
					<input type="submit" class="layui-btn submit" id="submit">

				</div>
			</div>
		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
	/**
	 *	加载分类下拉框
	 */
	$(function() {
		$.getJSON("publish/getSorts", function(data) {
			var str = "<option value='' selected='true' disabled='true'>请选择分类</option>";
			$(data).each(function() {
				str += "<option value='" + this.sort_id + "'>" + this.sort_name + "</option>";
			})
			$("#select").html(str);
		});
	});

	/* 头像上传*/
	function changepic() {
		$("#prompt3").css("display", "none");
		var reads = new FileReader();
		f = document.getElementById('file').files[0];
		reads.readAsDataURL(f);
		reads.onload = function(e) {
			document.getElementById('img3').src = this.result;
			$("#img3").css("display", "block");
		};
	}
	/*
	
	*/
	$("#submit").click(function() {
		var a1 = $("#file").val();
		var a2 = $("#spmc").val();
		var a3 = $("#spxq").val();
		var a4 = $("#spprice").val();
		var a5 = $("#select").val();
		var a6 = $("#jyaddress").val();
		var a7 = $("#phonenum").val();

		if (a2.length > 14) {
			alert("商品名称超过最大长度!");
			return false;
		}
		if (!(/^1[3456789]\d{9}$/.test(a7))) {
			alert("电话格式不正确!")
			return false;
		}
		if ((a1 != "") && (a2 != "") && (a3 != "") && (a4 != "") && (a5 != null) && (a6 != "") && (a7 != "")) {
			alert("发布成功");
			form.submit();
			return true;
		} else {
			alert("请填完整填写发布信息!");
			return false;
		}
	});
</script>
