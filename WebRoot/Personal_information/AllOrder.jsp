<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>所有订单</title>
<!--格式-->
<link
	href="//g.alicdn.com/tb/mtb-profile/0.0.2/oth/p/sns/1.0/tbsp-sns-min.css?t=20120401.css"
	type="text/css" rel="stylesheet" />

<link rel="stylesheet" href="../css/new_file.css" />
<link rel="stylesheet"
	href="//g.alicdn.com/tb/mtbframe/2.0.2/pages/home/base.css">
<link href="//g.alicdn.com//tb/mtb-profile/0.0.2/app-config.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../css/public.css" />
<link rel="stylesheet" href="../css/layui.css" />
<style>
#privacySet {
	display: none
}

#page {
	margin-top: 40px;
}

.mt-menu {
	margin-left: 30px;
}

#all {
	z-index: 0;
	display: block;
}

#ed {
	z-index: 0;
	display: none;
}

#no {
	z-index: 0;
	display: none;
}

.shou {
	float: right;
	margin-right: 20px;
	margin-top: -40px;
}

.ulgeshi {
	width: 847px;
	height: 40px;
	line-height: 40px;
	text-align: center;
	background: #90ee90;
	border: 1px solid;
	border-bottom: 1px solid blue;
	margin-top: -19px;
	margin-left: -92px;
	background: #90ee90;
}

.ulgeshi_1 {
	width: 847px;
	height: 40px;
	line-height: 40px;
	text-align: center;
	background: #90ee90;
	border: 1px solid;
	border-bottom: 1px solid blue;
	background: #90ee90;
}

.ulgeshi_2 {
	width: 847px;
	height: 150px;
	line-height: 150px;
	text-align: center;
	border-left: 1px solid;
	border-right: 1px solid;
	border-bottom: 1px solid;
}

.li {
	text-align: center;
}

.baobei {
	width: 350px;
	float: left;
	margin-left: -60px;
	
}
.baobei img{
	width: 100px;
	height: 100px;
	
}
.danjia {
	width: 75px;
	float: left;
}

.shuliang {
	width: 75px;
	float: left;
}

.jiaoyizhuangtai {
	width: 150px;
	float: left;
}

.jiaoyicaozuo {
	width: 190px;
	float: left;
}

.tb {
	padding-top: 50px;
}

.divgeshi {
	margin-top: 50px;
}

.th_l {
	float: left;
	margin-left: 20px;
}

.th_r {
	float: right;
	margin-right: 20px;
	clear: right;
}

.th_r img {
	filter: grayscale(100%);
	opacity: 0.6;
}

.th_r img:hover {
	filter: none;
	opacity: 1.0;
}
.g_name{
	width: 100px;
	height: 30px;
	margin-left:230px;
	margin-top:-120px;
	line-height:normal;
	float: left;
	white-space: pre-wrap;
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	word-wrap: break-word;
}
</style>
<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	// 三个div之间的隐藏和显示
	$(function() {
		$(".allorder").click(function() {
			$(".allorder").addClass("selected");
			$(".ordered").removeClass("selected");
			$(".noorder").removeClass("selected");
			$("#all").css("display", "block");
			$("#ed").css("display", "none");
			$("#no").css("display", "none");
		})

		$(".ordered").click(function() {
			$(".allorder").removeClass("selected");
			$(".ordered").addClass("selected");
			$(".noorder").removeClass("selected");
			$("#all").css("display", "none");
			$("#ed").css("display", "block");
			$("#no").css("display", "none");
		})

		$(".noorder").click(function() {
			$(".allorder").removeClass("selected");
			$(".ordered").removeClass("selected");
			$(".noorder").addClass("selected");
			$("#all").css("display", "none");
			$("#ed").css("display", "none");
			$("#no").css("display", "block");
		})

		// 根据u_id接收到了数据库的订单库
		$.getJSON("../trade/gettrade", {
			"u_id" : '${sessionScope.user.u_id}',
		}, function(data) {
			var str = "";
			$.each(data, function() {
				if (this.receive_id == "已收货") {
					str += "<div class='divgeshi'><ul class='ulgeshi_1'><li class='th_l'>" + this.createtime + "  订单号:" + this.out_trade_no
						+ "</li><li class='th_r'><img alt='删除' onclick=\"deletetrade(" + this.out_trade_no + ",'" + this.receive_id + "')\" src='../images/dustbin.png' style='width: 25px; height: 25px;'></li></ul>"
						+ "<ul class='ulgeshi_2'><li style='word-break:break-all' class='baobei'><img src='../" + this.goods.g_img + "'/><div class='g_name'>" + this.goods.g_name + "</div></li><li class='danjia'>￥" + this.total_amount
						+ "</li><li class='shuliang'>" + this.number + "</li><li class='jiaoyizhuangtai'>" + this.receive_id
						+ "</li><li class='jiaoyicaozuo'>再次购买</li></ul></div>"
				} else if (this.receive_id == "未收货") {
					str += "<div class='divgeshi'><ul class='ulgeshi_1'><li class='th_l'>" + this.createtime + "    订单号:" + this.out_trade_no
						+ "</li><li class='th_r'><img alt='删除' onclick=\"deletetrade(" + this.out_trade_no + ",'" + this.receive_id + "')\" src='../images/dustbin.png' style='width: 25px; height: 25px;'></li></ul>"
						+ "<ul class='ulgeshi_2'><li style='word-break:break-all' class='baobei'><img src='../" + this.goods.g_img + "'/><div class='g_name'>" + this.goods.g_name + "</div></li><li class='danjia'>￥" + this.total_amount
						+ "</li><li class='shuliang'>" + this.number + "</li><li class='jiaoyizhuangtai'>" + this.receive_id
						+ "</li><li class='jiaoyicaozuo'>再次购买</li></ul><input type='button' value='确认收货' class='shou' onclick=\"changetrade(" + this.out_trade_no + ")\"/></div>"
				}
			})

			$("#neirongzaihouall").append(str);

		})
		/*确认订单的显示  */
		$.getJSON("../trade/getedtrade", {
			"u_id" : '${sessionScope.user.u_id}',
		}, function(data) {
			var str = "";
			$(data).each(function() {

				str += "<div class='divgeshi'><ul class='ulgeshi_1'><li class='th_l'>" + this.createtime + "  订单号:" + this.out_trade_no
						+ "</li><li class='th_r'><img alt='删除' onclick=\"deletetrade(" + this.out_trade_no + ",'" + this.receive_id + "')\" src='../images/dustbin.png' style='width: 25px; height: 25px;'></li></ul>"
						+ "<ul class='ulgeshi_2'><li style='word-break:break-all' class='baobei'><img src='../" + this.goods.g_img + "'/><div class='g_name'>" + this.goods.g_name + "</div></li><li class='danjia'>￥" + this.total_amount
						+ "</li><li class='shuliang'>" + this.number + "</li><li class='jiaoyizhuangtai'>" + this.receive_id
						+ "</li><li class='jiaoyicaozuo'>再次购买</li></ul></div>"
			})

			$("#neirongzaihoued").append(str);

		})
		/*未确认订单的显示  */
		$.getJSON("../trade/getnotrade", {
			"u_id" : '${sessionScope.user.u_id}',
		}, function(data) {
			var str = "";
			$(data).each(function() {
				str += "<div class='divgeshi'><ul class='ulgeshi_1'><li class='th_l'>" + this.createtime + "    订单号:" + this.out_trade_no
						+ "</li><li class='th_r'><img alt='删除' onclick=\"deletetrade(" + this.out_trade_no + ",'" + this.receive_id + "')\" src='../images/dustbin.png' style='width: 25px; height: 25px;'></li></ul>"
						+ "<ul class='ulgeshi_2'><li style='word-break:break-all' class='baobei'><img src='../" + this.goods.g_img + "'/><div class='g_name'>" + this.goods.g_name + "</div></li><li class='danjia'>￥" + this.total_amount
						+ "</li><li class='shuliang'>" + this.number + "</li><li class='jiaoyizhuangtai'>" + this.receive_id
						+ "</li><li class='jiaoyicaozuo'>再次购买</li></ul><input type='button' value='确认收货' class='shou' onclick=\"changetrade(" + this.out_trade_no + ")\"/></div>"
			})

			$("#neirongzaihouno").append(str);

		})




	})
	// 确认收货的方法
	function changetrade(out_trade_no) {
		if (confirm("确认收货吗？")) {
			$.getJSON("../trade/changetrade", {
				"out_trade_no" : out_trade_no
			}, function(data) {
				if (data) {
					alert("订单确认收货成功！");
					window.location.href = "AllOrder.jsp";
				} else {
					alert("订单确认收货失败！");
				}
			});
		}

	}
	// 删除订单的方法
	function deletetrade(out_trade_no, receive_id) {
		if (confirm("确认删除订单吗？")) {
			if (receive_id == "已收货") {
				$.getJSON("../trade/deletetrade", {
					"out_trade_no" : out_trade_no
				}, function(data) {
					if (data) {
						alert("订单删除成功！");
						window.location.href = "AllOrder.jsp";
					} else {
						alert("订单删除失败！");
					}
				});
			} else {
				alert("订单还未确认收货！不可删除！ ");
			}


		}

	}
</script>

</head>

<body class="mission  mytaobao-v2 ">
	<%@ include file="header.jsp"%>
	<div id="page">
		<script type="text/javascript"
			src="//g.alicdn.com/tb/mtbframe/2.0.4/components/common/base.js"></script>

		<div id="content" class="layout grid-s160m0">
			<div id="mytaobao-panel" class="grid-c2">
				<div class="col-main">
					<div class="main-wrap">
						<div id="profile" class="sns-config">
							<div class="sns-tab tab-app">
								<ul>
									<li class="selected">
										<h3>个人资料</h3>
									</li>
								</ul>
								<ul class="tab-sub">
									<li class="allorder selected"><a><span>所有订单</span></a></li>
									<li class="ordered"><a><span>已收货</span></a></li>
									<li class="noorder"><a><span>未收货</span></a></li>
								</ul>
							</div>
							<div class="sns-box box-detail" id="all">
								<div class="bd">
									<div class="sns-nf">
										<div id="main-profile" class="parts all">
											<!--内容-->
											<ul class="ulgeshi" id="neirongzaihouall">
												<li class="baobei">宝贝</li>
												<li class="danjia">单价</li>
												<li class="shuliang">数量</li>
												<li class="jiaoyizhuangtai">交易状态</li>
												<li class="jiaoyicaozuo">交易操作</li>
											</ul>

										</div>
									</div>
								</div>
							</div>
							<div class="sns-box box-detail" id="ed">
								<div class="bd">
									<div class="sns-nf">
										<div id="main-profile" class="parts ed">
											<!--内容-->
											<ul class="ulgeshi" id="neirongzaihoued">
												<li class="baobei">宝贝</li>
												<li class="danjia">单价</li>
												<li class="shuliang">数量</li>
												<li class="jiaoyizhuangtai">交易状态</li>
												<li class="jiaoyicaozuo">交易操作</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="sns-box box-detail" id="no">
								<div class="bd">
									<div class="sns-nf">
										<div id="main-profile" class="parts no">
											<!--内容-->
											<ul class="ulgeshi" id="neirongzaihouno">
												<li class="baobei">宝贝</li>
												<li class="danjia">单价</li>
												<li class="shuliang">数量</li>
												<li class="jiaoyizhuangtai">交易状态</li>
												<li class="jiaoyicaozuo">交易操作</li>
											</ul>

										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-sub" style="z-index:0">
					<aside class="mt-menu" id="J_MtSideMenu">
						<div class="mt-menu-tree">
							<dl class="mt-menu-item mt-account-manage no-decoration">
								<dt>账号管理</dt>
								<dd>
									<!--头像-->
									<img src="../${sessionScope.user.u_touxiang}"
										style="width: 60px;height:60px;">
								</dd>
								<dd>
									<a href="Personal_information.jsp">个人资料</a>
								</dd>
								<dd>
									<a href="UpdatePwd.jsp">修改密码</a>
								</dd>
								<dd>
									<a href="AllOrder.jsp" style="color: red;">我的订单</a>
								</dd>
								<dd>
									<a href="Address_Detail.jsp">收货地址</a>
								</dd>
								<dd>
									<a href="Assess.jsp">我的商品</a>
								</dd>
							</dl>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-wrap">
		<div class="footer-bottom">
			<p class="friend-link">
				<span>友情链接：</span>
				<!-- <a href="">淘二淘</a> -->

				<a href="http://wanlinqiang.com?from=taoertao" target="_blank">万林强的博客</a>

				<a href="http://www.taoertao.com" target="_blank">校园二手街</a> <a
					href="https://portal.qiniu.com/signup?code=3latfmv9iksb6"
					target="_blank">七牛云</a> <a href="http://www.taoertao.com"
					target="_blank">大学生二手网</a> <a
					href="https://www.vultr.com/?ref=7549292" target="_blank">免备案服务器</a>

				<a href="http://www.jpfuli.com?from=taoertao" target="_blank">极品福利</a>

				<a href="http://vip.sucai.tv/" target="_blank">免费VIP视频</a> <a
					href="http://www.mosenx.com/?from=taoertao" target="_blank">墨森运动</a>

			</p>
			<p class="column">
				<a href="/service/about">关于我们</a> <a href="/service/problem">常见问题</a>
				<a href="/user/help">意见反馈</a> <a href="/service/protocol">服务协议</a> <a
					href="/service/contect">联系我们</a>
				<script
					src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214"
					language="JavaScript"></script>
				<script>
					var _hmt = _hmt || [];
					(function() {
						var hm = document.createElement("script");
						hm.src = "https://hm.baidu.com/hm.js?b43531d7c229bad3bcbfbc7991208c60";
						var s = document.getElementsByTagName("script")[0];
						s.parentNode.insertBefore(hm, s);
					})();
				</script>
			</p>
			<p class="tips">本站所有信息均为用户自由发布，本站不对信息的真实性负任何责任，交易时请注意识别信息的真假如有网站内容侵害了您的权益请联系我们删除，举报QQ：584845663</p>
			<!--<p><span>举报QQ：584845663</span>　<span>商务邮箱：584845663@qq.com</span>　<script src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214" language="JavaScript"></script></p>-->
			<p class="right">
				<span>Copyright © 2014-2017, Taoertao.com, All Rights
					Reserved</span> <a target="_blank" href="http://www.miitbeian.gov.cn/">浙ICP备16002812号</a>
				<a class="beian" target="_blank"
					href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=33011802000633"
					style="display:inline-block;height:20px;line-height:20px;">浙公网安备
					33011802000633号</a> <span>商务邮箱：584845663@qq.com</span> <a
					href="http://webscan.360.cn/index/checkwebsite/url/new.taoertao.com"
					target="_blank"><img border="0"
					src="http://webscan.360.cn/img/logo_verify.png" /></a>
			</p>
		</div>
	</div>

</body>

</html>