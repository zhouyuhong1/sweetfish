<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>个人资料</title>
<!--格式-->
<link
	href="//g.alicdn.com/tb/mtb-profile/0.0.2/oth/p/sns/1.0/tbsp-sns-min.css?t=20120401.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../css/public.css" />
<link rel="stylesheet" href="../css/layui.css" />
<link rel="stylesheet" href="../../css/new_file.css" />
<link rel="stylesheet" href="../css/UpdatePwd.css" />
<style>
#privacySet {
	display: none
}

#page {
	margin-top: 40px;
}

td {
	font-size: 15px;
	height: 28px;
}

.mt-menu {
	margin-left: 30px;
}

input {
	border: 0px;
}

.queding {
	display: none;
	margin-left: 200px;
}

.bianji {
	margin-left: 130px;
}
</style>
<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	$(function() {
		$(".bianji").click(function() {
			$("input").css("border", "1px solid");
			var nicheng = document.getElementById("nicheng");
			var nianling = document.getElementById("nianling");
			nicheng.readOnly = false;
			nianling.readOnly = false;
			$(".queding").css("display", "block");
			$(".bianji").css("display", "none");
		})

		$(".queding").click(function() {
			$("input").css("border", "0px");
			var nicheng = document.getElementById("nicheng");
			var nianling = document.getElementById("nianling");
			nicheng.readOnly = true;
			nianling.readOnly = true;
			$(".bianji").css("display", "block");
			$(".queding").css("display", "none");
			// 修改完更新数据库
			var name = $("#nicheng").val();
			var age = $("#nianling").val();
			var tel = $("#dianhua").val();
			var user ={
					"u_phone":'${sessionScope.user.u_phone}',
					"u_password":'${sessionScope.user.u_password}',
					"u_name":name,
					"u_age":age,
			}
			$.ajax({
				url:"../user/update",
				type:"post",
				data:user,
				async:false,
				datatype:"json",
				success:function(data){
					if(data){
						alert("修改成功");
						window.location.href="Personal_information.jsp";
					}else {
						alert("修改失败");
					}
				}

			})
		})
	})
</script>
</head>

<body class="mission  mytaobao-v2 ">
	<%@ include file="header.jsp"%>
	<div id="page">
		<link rel="stylesheet"
			href="//g.alicdn.com/tb/mtbframe/2.0.2/pages/home/base.css">
		<script type="text/javascript"
			src="//g.alicdn.com/tb/mtbframe/2.0.4/components/common/base.js"></script>

		<div id="content" class="layout grid-s160m0">
			<div id="mytaobao-panel" class="grid-c2">
				<link href="//g.alicdn.com//tb/mtb-profile/0.0.2/app-config.css"
					type="text/css" rel="stylesheet" />

				<div class="col-main">
					<div class="main-wrap">
						<div id="profile" class="sns-config">
							<div class="sns-tab tab-app">
								<ul>
									<li class="selected">
										<h3>个人资料</h3>
									</li>
								</ul>
								<ul class="tab-sub">
									<li class="selected"><a
										href="pages/Personal_information.jsp"><span>基本资料</span></a></li>
								</ul>
							</div>
							<div class="sns-box box-detail">
								<div class="bd">
									<div class="sns-nf">
										<div id="main-profile" class="parts"
											style=" margin-top: -30px;">
											<div class="middle_right_xia">
												<table class="table">
													<tr>
														<td class="mar" style="text-align: right;">昵称：</td>
														<td class="nicheng">
															<!--接收登陆者昵称  --> <input type="text" id="nicheng"
															value="${sessionScope.user.u_name }" readOnly />
														</td>
													</tr>
													<tr>
														<td class="mar" style="text-align: right;">性別：</td>
														<td>
															<!--接收登陆者真实姓名  --> <input type="text" id="xingbie"
															value="${sessionScope.user.u_sex }" readOnly />
														</td>
													</tr>
													<tr>
														<td class="mar" style="text-align: right;">年齡：</td>
														<td>
															<!--接收登陆者性别  --> <input type="text" id="nianling"
															value="${sessionScope.user.u_age }" readOnly />
														</td>
													</tr>
													<tr>
														<td class="mar" style="text-align: right;">电话号码：</td>
														<td>
															<!--接收登陆者身份证号码  --> <input type="text" id="dianhua"
															value="${sessionScope.user.u_phone }" readOnly />
														</td>
													</tr>
													<tr class="bianjitr">
														<td class="mar" colspan="2">
															<button class="bianji">编辑资料</button>
															<button class="queding">确认</button>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-sub" style="z-index:0">
					<aside class="mt-menu" id="J_MtSideMenu">
						<div class="mt-menu-tree">
							<dl class="mt-menu-item mt-account-manage no-decoration">
								<dt>账号管理</dt>
								<dd>
									<!--头像-->
									<img src="../${sessionScope.user.u_touxiang}"
										style="width: 60px;height:60px;">
								</dd>
								<dd>
									<a href="Personal_information.jsp" style="color: red;">个人资料</a>
								</dd>
								<dd>
									<a href="UpdatePwd.jsp">修改密码</a>
								</dd>
								<dd>
									<a href="AllOrder.jsp">我的订单</a>
								</dd>
								<dd>
									<a href="Address_Detail.jsp">收货地址</a>
								</dd>
								<dd>
									<a href="Assess.jsp">我的商品</a>
								</dd>
							</dl>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-wrap">
		<div class="footer-bottom">
			<p class="friend-link">
				<span>友情链接：</span>
				<!-- <a href="">淘二淘</a> -->

				<a href="http://wanlinqiang.com?from=taoertao" target="_blank">万林强的博客</a>

				<a href="http://www.taoertao.com" target="_blank">校园二手街</a> <a
					href="https://portal.qiniu.com/signup?code=3latfmv9iksb6"
					target="_blank">七牛云</a> <a href="http://www.taoertao.com"
					target="_blank">大学生二手网</a> <a
					href="https://www.vultr.com/?ref=7549292" target="_blank">免备案服务器</a>

				<a href="http://www.jpfuli.com?from=taoertao" target="_blank">极品福利</a>

				<a href="http://vip.sucai.tv/" target="_blank">免费VIP视频</a> <a
					href="http://www.mosenx.com/?from=taoertao" target="_blank">墨森运动</a>

			</p>
			<p class="column">
				<a href="/service/about">关于我们</a> <a href="/service/problem">常见问题</a>
				<a href="/user/help">意见反馈</a> <a href="/service/protocol">服务协议</a> <a
					href="/service/contect">联系我们</a>
				<script
					src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214"
					language="JavaScript"></script>
				<script>
					var _hmt = _hmt || [];
					(function() {
						var hm = document.createElement("script");
						hm.src = "https://hm.baidu.com/hm.js?b43531d7c229bad3bcbfbc7991208c60";
						var s = document.getElementsByTagName("script")[0];
						s.parentNode.insertBefore(hm, s);
					})();
				</script>
			</p>
			<p class="tips">本站所有信息均为用户自由发布，本站不对信息的真实性负任何责任，交易时请注意识别信息的真假如有网站内容侵害了您的权益请联系我们删除，举报QQ：584845663</p>
			<!--<p><span>举报QQ：584845663</span>　<span>商务邮箱：584845663@qq.com</span>　<script src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214" language="JavaScript"></script></p>-->
			<p class="right">
				<span>Copyright © 2014-2017, Taoertao.com, All Rights
					Reserved</span> <a target="_blank" href="http://www.miitbeian.gov.cn/">浙ICP备16002812号</a>
				<a class="beian" target="_blank"
					href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=33011802000633"
					style="display:inline-block;height:20px;line-height:20px;">浙公网安备
					33011802000633号</a> <span>商务邮箱：584845663@qq.com</span> <a
					href="http://webscan.360.cn/index/checkwebsite/url/new.taoertao.com"
					target="_blank"><img border="0"
					src="http://webscan.360.cn/img/logo_verify.png" /></a>
			</p>
		</div>
	</div>
</body>

</html>