<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>商品详情</title>
    

	<link rel="stylesheet" href="css/public.css">
    <link rel="stylesheet" href="css/detail.css">
  	<script type="text/javascript" src="js/jquery.min.js"></script>
</head>

<body>
  <jsp:include page="header.jsp"></jsp:include>
  <jsp:include page="headerSearh.jsp"></jsp:include>
          <div id="main" class="clearfix">
			<div class="detail fl clearfix">
				<div class="title">
					<!-- <h2>商品标题商品标题</h2> -->
					<span class="publish-time fl" id="createtime">发布于：2019-05-08 23:23:18</span>
					<span class="view-number fl">1809次浏览</span>

					<span class="view-number fl">20人想要</span>

					<a href="javascript:;" class="report fr">举报</a>
				</div>

				<div id="MagnifierWrap2" class="fl">
					<div class="MagnifierMain">
						<!-- <img class="MagTargetImg" src="http://img01.taobaocdn.com/bao/uploaded/i1/TB1nlLPOXXXXXcwXpXXXXXXXXXX_!!0-item_pic.jpg"> -->

						<img class="MagTargetImg" src="">

					</div>
				</div>
				<div class="good-info fr">
					<h2 id="goodsName">敦煌劫余录</h2>
					<div class="info-line">
						<span class="param-name">价格</span> ￥
						<span class="param-value good-price" id="goodsprice">200</span>
					</div>
					<div class="info-line">
						<span class="param-name">原价</span>
						<span class="param-value good-old-price" id="goodsprice2">￥220</span>

					</div>

					<div class="info-line">
						<span class="param-name">卖家</span>
						<span class="param-value" id="maijia"></span>
					</div>

					<div class="info-line">
						<span class="param-name">交易方式</span>

						<span class="param-value">在线交易</span>
					</div>
					<input type="hidden" value="1" id="phone">
					<input type="hidden" value="-1" id="u_id">
					<div class="info-line btns">
						<a href="#" class="info-btn contect-seller" id="goumai">点击购买</a>
					</div>
				</div>
				<div class="good-description">
					<div class="description-title"><span>二货描述</span></div>
					<div class="description-body">
						<p id="content">
						</p>
					</div>
				</div>
			</div>
			<div class="detail-sidebar fr">
				<div class="sidebar-header">推荐二货</div>
				<ul class="sidebar-list">

					<li>
						<a class="sidebar clearfix">
							<img class="sidebar-image fl" src="http://res.new.taoertao.com/upload/goods/20195/upload_44671bc55957d33d479a8bcc7c18ea50.jpg?imageView2/1/w/200/h/200/q/100" alt="敦煌劫余录">
							<p class="sidebar-title">敦煌劫余录</p>
							<p class="sidebar-price">￥200</p>
						</a>
					</li>

					<li>
						<a class="sidebar clearfix">
							<img class="sidebar-image fl" src="http://res.new.taoertao.com/upload/goods/20185/1527388317237290342.png?imageView2/1/w/200/h/200/q/100" alt="考研书籍打包卖">
							<p class="sidebar-title">考研书籍打包卖</p>
							<p class="sidebar-price">￥100</p>
						</a>
					</li>

					<li>
						<a class="sidebar clearfix">
							<img class="sidebar-image fl" src="http://res.new.taoertao.com/upload/goods/20183/upload_539735566634f94bfb5ccea5a289e4ba.jpg?imageView2/1/w/200/h/200/q/100" alt="中国近代史纲要">
							<p class="sidebar-title">中国近代史纲要</p>
							<p class="sidebar-price">￥12</p>
						</a>
					</li>

					<li>
						<a class="sidebar clearfix">
							<img class="sidebar-image fl" src="http://res.new.taoertao.com/upload/goods/20182/1518497467626106344.png?imageView2/1/w/200/h/200/q/100" alt="初中美术教材+教师用书+碟">
							<p class="sidebar-title">初中美术教材+教师用书+碟</p>
							<p class="sidebar-price">￥265</p>
						</a>
					</li>
				</ul>
			</div>
		</div> 
        <jsp:include page="footer.jsp"></jsp:include>
        <script type="text/javascript">
        	$(function(){
        		
        		var g_id = <%= request.getParameter("g_id")%>
        		if(g_id!=null||g_id!=""){
        		$.ajaxSettings.async = false;
        		$.getJSON("goods/selectGoods",{"id":g_id},function(data){
        			var goods = data;
        			$("#createtime").html("发布于："+goods.create_time);
        			$("#goodsName").html(goods.g_name);
        			$("#goodsprice").html(goods.g_price);
        			$("#goodsprice2").html("￥"+goods.g_price);
        			$(".MagTargetImg").attr("src",goods.g_img)
        			$("#u_id").val(goods.m_id);
        			$("#content").html(goods.text);
        			$.getJSON("user/getuser",{"g_id":g_id},function(data){
        				$("#maijia").html(data.u_name);
        				$("#phone").val(data.u_phone);
        			});
        		});
        		}
        		
        		// 点击购买按钮
        		$("#goumai").click(function(){
        			var in_name =  '${sessionScope.user.u_name}';
					var in_phone =$("#phone").val();
					var in_money = $("#goodsprice").html();
					var u_id=${sessionScope.user.u_id};
					/* window.location.href = "pay/to_alipay?in_name=" + in_name
						+ "&&in_phone=" + in_phone + "&&in_money=" + in_money +"&&u_id="+u_id+"&&g_id="+g_id; */
					window.location.href = "buyadress.jsp?in_money=" + in_money +"&&u_id="+u_id+"&&g_id="+g_id;
        		});
        		
        	});
        </script>
        
</body>
</html>