package com.kgc.entity;

public class Publish {
	private int shen_id;
	private String shen_name;
	private int sort_id;
	private double shen_price;
	private String shen_img;
	private String shen_text;
	private String shen_createtime;
	private int u_id;
	private int shen_statement;
	
	
	public int getShen_id() {
		return shen_id;
	}
	public void setShen_id(int shen_id) {
		this.shen_id = shen_id;
	}
	public String getShen_name() {
		return shen_name;
	}
	public void setShen_name(String shen_name) {
		this.shen_name = shen_name;
	}
	public int getSort_id() {
		return sort_id;
	}
	public void setSort_id(int sort_id) {
		this.sort_id = sort_id;
	}
	public double getShen_price() {
		return shen_price;
	}
	public void setShen_price(double shen_price) {
		this.shen_price = shen_price;
	}
	public String getShen_img() {
		return shen_img;
	}
	public void setShen_img(String shen_img) {
		this.shen_img = shen_img;
	}
	public String getShen_text() {
		return shen_text;
	}
	public void setShen_text(String shen_text) {
		this.shen_text = shen_text;
	}
	
	
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public String getShen_createtime() {
		return shen_createtime;
	}
	public void setShen_createtime(String shen_createtime) {
		this.shen_createtime = shen_createtime;
	}
	public int getShen_statement() {
		return shen_statement;
	}
	public void setShen_statement(int shen_statement) {
		this.shen_statement = shen_statement;
	}
	
	
}
