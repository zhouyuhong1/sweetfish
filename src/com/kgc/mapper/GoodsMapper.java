package com.kgc.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kgc.entity.Goods;

public interface GoodsMapper {
	
	/**
	 * 分页
	 * @param map
	 * @return
	 */
	public List<Goods> findByPage(HashMap<String, Object> map);
	
	/**
     * 查询用户记录总数
     * @return
     */
    public int selectCount();
    
    /**
     * 查询所有用户数据
     * @return
     */
    public List<Goods> selectUserList();

    /*
     * 搜索商品
     */
    public List<Goods> queryGoods(@Param("name") String name);
    
    /*
     * 查询对应ID商品详情
     */
    public Goods selectGoods(@Param("id") int id);
    
    int updateGoodsZt(@Param("g_id")int g_id,@Param("zt_id")int zt_id);
    
}
