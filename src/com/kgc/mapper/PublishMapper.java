package com.kgc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kgc.entity.Goods;
import com.kgc.entity.Publish;
import com.kgc.entity.Sort;

public interface PublishMapper {
	// 发布商品加入publish库
	int publish(@Param("publish")Goods publish);
	// 审核通过后，发布的商品进入goods库
	
	// 根据u_id读取publish库里面所有的数据到页面
	public List<Goods> getpublish(@Param("u_id")int u_id);
	
	public List<Goods> getedpublish(@Param("u_id")int u_id);
	
	public List<Goods> getnopublish(@Param("u_id")int u_id);
	
	public List<Goods> getingpublish(@Param("u_id")int u_id);
	
	public List<Goods> getpublishjie(@Param("u_id")int u_id);
	
	//取消订单
	public int quxiaopublish(@Param("shen_id")int shen_id);
	// 分类的下拉框
	List<Sort> getSort();
	
}
