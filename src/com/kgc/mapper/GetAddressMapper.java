package com.kgc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kgc.entity.Address;

public interface GetAddressMapper {
	//添加地址
	public int insertaddress(@Param("address")Address address);
	
	
	public List<Address> getaddress(@Param("u_id")int u_id);
	
	public int deleteaddress(@Param("gad_id")Integer gad_id);
	
	public List<Address> getInfo(@Param("u_id") Integer u_id);
}
