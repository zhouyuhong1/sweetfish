package com.kgc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kgc.entity.Goods;
import com.kgc.service.SeachService;

@Controller
@RequestMapping("seach")
@ResponseBody
public class SeachController {
	
	@Autowired
	private SeachService seachService;
	
	@RequestMapping("queryBySort")
	public List<Goods> queryBySort(int sort_id){
		
		return seachService.queryBySort(sort_id);
	}

}
