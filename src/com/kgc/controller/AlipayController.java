package com.kgc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.kgc.entity.AlipayAttr;
import com.kgc.entity.Trade;
import com.kgc.service.GoodsService;
import com.kgc.service.TradeService;

@Controller
@RequestMapping("pay")
public class AlipayController {

	@Autowired
	private TradeService tradeService;
	
	@Autowired
	private GoodsService goodsService;
	
	@RequestMapping("to_alipay")
	public void pay(HttpServletRequest request,HttpServletResponse response) throws IOException {
		// 处理乱码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		/*获得初始化的AlipayClient
		 * AlipayClient alipayClient = new DefaultAlipayClient(
		 * "https://openapi.alipaydev.com/gateway.do",
		 * "2016091700535499","请复制第1步中生成的密钥中的商户应用私钥,
		 * "json","utf-8","沙箱环境RSA2支付宝公钥","RSA2");
		 * */
		AlipayClient alipayClient = new DefaultAlipayClient(
				AlipayAttr.gatewayUrl, AlipayAttr.app_id,
				AlipayAttr.merchant_private_key, "json", AlipayAttr.charset,
				AlipayAttr.alipay_public_key, AlipayAttr.sign_type);

		// 取购买人名称
		String in_name = request.getParameter("in_name");
		// 取手机号
		String in_phone = request.getParameter("in_phone");
		// 创建唯一订单号
		
		
		int random = (int) (Math.random() * 10000);
		String dateStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

		// 订单号拼接规则：手机号后四位+当前时间后四位+随机数四位数
		String out_trade_no = in_phone.substring(7) + dateStr.substring(10)
				+ random;
		// 拼接订单名称
//		String subject = in_name + "大喵的订单";
		String subject = in_name+"的订单";

		// 取付款金额
		String total_amount = request.getParameter("in_money");

		// 设置请求参数
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
		alipayRequest.setReturnUrl(AlipayAttr.return_url);//支付成功响应后跳转地址
		alipayRequest.setNotifyUrl(AlipayAttr.notify_url);//异步请求地址

		/*FAST_INSTANT_TRADE_PAY 二维码瞬时支付
		 * out_trade_no 订单号 total_amount 订单金额  subject 订单名称
		 */
		alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no
				+ "\"," + "\"total_amount\":\"" + total_amount + "\","
				+ "\"subject\":\"" + subject + "\"," + "\"body\":\""
				+ ""+ "\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
		String result = "请求无响应";
		// 请求
		try {
			// 创建订单，插入订单表
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String nowtime = sdf.format(date);
			
			Trade trade = new Trade();
			trade.setOut_trade_no(out_trade_no);
			trade.setTotal_amount(total_amount);
			trade.setTradestates_id(2);
			trade.setCreatetime(nowtime);
			trade.setG_id(Integer.parseInt(request.getParameter("g_id")));
			trade.setU_id(Integer.parseInt(request.getParameter("u_id")));
			
			tradeService.createTrade(trade);
			System.out.println("订单创建成功!");
			//通过阿里客户端，发送支付页面请求
			result = alipayClient.pageExecute(alipayRequest).getBody();
			response.getWriter().println(result);
			response.getWriter().flush();
		} catch (AlipayApiException e) {
			e.printStackTrace();
		} finally {
			response.getWriter().close();
		}
	}
	
	@RequestMapping("alipay_return")
	public String alipay_return(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		// 获取支付宝回调反馈的信息
		Map<String, String> params = new HashMap<String, String>();
		Map<String, String[]> requestParams = request.getParameterMap();
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter
				.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			// 乱码解决，这段代码在出现乱码时使用
			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}

		boolean signVerified = false;
		try {// 调用SDK验证签名
			signVerified = AlipaySignature.rsaCheckV1(params,
					AlipayAttr.alipay_public_key, AlipayAttr.charset,
					AlipayAttr.sign_type);
		} catch (AlipayApiException e) {
			e.printStackTrace();
		} 

		// 请在这里编写您的程序（以下代码仅作参考）
		if (signVerified) {//验签成功后执行的自定义业务代码
			// 商户订单号
			String out_trade_no = new String(request.getParameter(
					"out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
			// 支付宝交易号
			String trade_no = new String(request.getParameter("trade_no")
					.getBytes("ISO-8859-1"), "UTF-8");
			// 付款金额
			String total_amount = new String(request.getParameter(
					"total_amount").getBytes("ISO-8859-1"), "UTF-8");
			String msg = "支付宝交易号:" + trade_no + "<br/>商户订单号"
					+ out_trade_no + "<br/>付款金额:" + total_amount;
			System.out.println(msg);
			
			int g_id = tradeService.idByGoods(out_trade_no);
			//修改订单表的支付状态
			tradeService.updateTrade(out_trade_no,trade_no,1,1);
			goodsService.updateGoods(g_id, 2);
			return "redirect:/Personal_information/AllOrder.jsp";

		} else {
			response.getWriter().println("验签/支付失败");
			response.getWriter().flush();
			response.getWriter().close();
			
		}
		return "error";
		
	}

}
