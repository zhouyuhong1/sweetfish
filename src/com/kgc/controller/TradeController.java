package com.kgc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kgc.entity.Goods;
import com.kgc.entity.Trade;
import com.kgc.service.TradeService;

@Controller
@RequestMapping("trade")
public class TradeController {
	
	@Autowired
	private TradeService tradeService;
	
	@RequestMapping("createtrade")
	public String createtrade(HttpServletRequest request,HttpServletResponse response){
		Trade trade = (Trade) request.getAttribute("trade");
		boolean flag=tradeService.createTrade(trade);
		if(flag){
			return "shouye";
		}else{
			return "error";
		}
	}
	
	@RequestMapping("gettrade")
	@ResponseBody
	public List<Trade> gettrade(int u_id){
		return tradeService.gettrade(u_id);
		
	}
	
	
	@RequestMapping("changetrade")
	@ResponseBody
	public boolean changetrade(String out_trade_no){
		return tradeService.changetrade(out_trade_no);
		
	}
	
	@RequestMapping("deletetrade")
	@ResponseBody
	public boolean deletetrade(String out_trade_no){
		return tradeService.deletetrade(out_trade_no);
		
	}
	
	
	@RequestMapping("getedtrade")
	@ResponseBody
	public List<Trade> getedtrade(int u_id){
		return tradeService.getedtrade(u_id);
		
	}
	
	@RequestMapping("getnotrade")
	@ResponseBody
	public List<Trade> getnotrade(int u_id){
		return tradeService.getnotrade(u_id);
		
	}
	
	/*
	 * 查询支付商品的id
	 */
	@RequestMapping("idByGoods")
	@ResponseBody
	 public int idByGoods(String out_trade_no){
		 return tradeService.idByGoods(out_trade_no);
	 }
	
	/*
	 * 支付成功后删除商品
	 */
	@RequestMapping("deleteGood")
	@ResponseBody
	 public int deleteGood(int g_id){
		 return tradeService.deleteGood(g_id);
	 }
}
