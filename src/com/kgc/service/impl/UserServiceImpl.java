package com.kgc.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.User;
import com.kgc.mapper.UserMapper;
import com.kgc.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userLoginMapper;
	
	public User login(String phone,String password){
		return  userLoginMapper.login(phone, password);
	}

	@Override
	public User queryByPhone(String phone) {
		User user = userLoginMapper.queryByPhone(phone);
		return user;
	}

	@Override
	public boolean addUser(User user) {
		return userLoginMapper.addUser(user)>=1?true:false;
	}

	@Override
	public User getNameByG_id(int g_id) {
		
		return userLoginMapper.getNameByG_id(g_id);
	}

	@Override
	public boolean UpdatePwd(String phone, String oldpassword, String newpassword) {
		return userLoginMapper.UpdatePwd(phone,oldpassword,newpassword) == 1?true:false;
	}
	
	@Override
	public int yzPhone(String phone) {
		return userLoginMapper.yzPhone(phone);
	}

	@Override
	public boolean updatePaswd(String pwd, String phone) {
		return userLoginMapper.updatePaswd(pwd,phone)==1? true : false;
	}

	@Override
	public boolean update(User user) {
		return userLoginMapper.update(user) == 1?true:false;
	}
	
}
