package com.kgc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.Address;
import com.kgc.mapper.GetAddressMapper;
import com.kgc.service.GetAddressService;
@Service
public class GetAddressServiceImpl implements GetAddressService{
	@Autowired
	private GetAddressMapper addressmapper;
	@Override
	public boolean insertaddress(Address address) {
		return addressmapper.insertaddress(address) == 1?true:false;
	}
	@Override
	public boolean deleteaddress(Integer gad_id) {
		return  addressmapper.deleteaddress(gad_id) == 1?true:false;
	}
	
	@Override
	public List<Address> getInfo(int u_id) {
		return addressmapper.getInfo(u_id);
	}
	@Override
	public List<Address> getaddress(int u_id) {
		return addressmapper.getaddress(u_id);
	}
	
	

}
