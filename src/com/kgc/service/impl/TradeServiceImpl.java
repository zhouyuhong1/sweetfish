package com.kgc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.Goods;
import com.kgc.entity.Trade;
import com.kgc.mapper.TradeMapper;
import com.kgc.service.TradeService;

@Service
public class TradeServiceImpl implements TradeService {

	@Autowired
	private TradeMapper tradeMapper;
	
	@Override
	public boolean createTrade(Trade trade) {
		System.out.println(trade);
		return tradeMapper.createtrade(trade)==1;
	}

	@Override
	public boolean updateTrade(String out_trade_no,String trade_no, int flg,int receiveflag) {
		
		return tradeMapper.updatetrade(out_trade_no, trade_no, flg,receiveflag)==1;
	}

	@Override
	public List<Trade> gettrade(int u_id) {
		return tradeMapper.gettrade(u_id);
	}

	@Override
	public boolean changetrade(String out_trade_no) {
		return tradeMapper.changetrade(out_trade_no)==1?true:false;
	}

	@Override
	public List<Trade> getedtrade(int u_id) {
		return tradeMapper.getedtrade(u_id);
	}

	@Override
	public List<Trade> getnotrade(int u_id) {
		return tradeMapper.getnotrade(u_id);
	}

	@Override
	public boolean deletetrade(String out_trade_no) {
		return tradeMapper.deletetrade(out_trade_no)==1?true:false;
	}
	
	@Override
	public int deleteGood(int g_id) {
		return tradeMapper.deleteGood(g_id);
	}

	@Override
	public int idByGoods(String out_trade_no) {
		return tradeMapper.idByGoods(out_trade_no);
	}
}
