package com.kgc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.Goods;
import com.kgc.mapper.SeachMapper;
import com.kgc.service.SeachService;

@Service
public class SeachServiceImpl implements SeachService {

	@Autowired
	private SeachMapper seachMapper;
	
	public List<Goods> queryBySort(int sort_id) {
		
		return seachMapper.queryBySort(sort_id);
	}
	
	

}
