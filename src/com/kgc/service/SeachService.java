package com.kgc.service;

import java.util.List;

import com.kgc.entity.Goods;

public interface SeachService {
	
	List<Goods> queryBySort(int sort_id);
	
}
