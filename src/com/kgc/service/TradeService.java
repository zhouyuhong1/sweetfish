package com.kgc.service;

import java.util.List;

import com.kgc.entity.Goods;
import com.kgc.entity.Trade;

public interface TradeService {

	boolean createTrade(Trade trade);
	
	boolean updateTrade(String out_trade_no,String trade_no,int flg,int receiveflag);
	
	public List<Trade> gettrade(int u_id);
	
	public boolean changetrade(String out_trade_no);
	
	public boolean deletetrade(String out_trade_no);
	
	public List<Trade> getedtrade(int u_id);
	
	public List<Trade> getnotrade(int u_id);
	
	public int idByGoods(String out_trade_no);
	
	public int deleteGood(int g_id);
}
